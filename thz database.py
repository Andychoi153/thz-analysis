#Thz data base for frequency of maximum's time
import numpy as np
import motplotlib.pyplot as plt
import glob
import os
def import_data( mydir='E:\\' ) :
    k=range()
    txtpattern = os.path.join(mydir,'*.dat')
    files=glob.glob(txtpattern)
    for i in file :    
                k[0,:] = 'E:\\i'
                print(k)
    return k


def reading_data_ext_data(k):
    x_max = range()
    y_max = range()
    x_mmgap = range()
    y_mmgap = range()
    for i in k:
        x, y = np.loadtxt(k[i],delimiter='\t',usecols=(0,1),unpack=True)
        x_max[i] = x[np.argmax(y)]
        y_max[i] = np.amax(y)
        x_mmgap[i] = x[np.argmax(y)] - x[np.argmin(y)]
        y_mmgap[i] = np.amax(y)-np.amin(y)
    x_max*=20/3
    x_mmgap*=20/3    
    return x_max, y_max, x_mmgap, y_mmgap

def main():
    
    import_data() = k
    reading_data_ext_data(k) = x_max, y_max, x_mmgap, y_mmgap
           
    plt.subplot(2,1,1)
    plt.plot(x_max.size, x_max) #linspace 추가할것
    plt.title('Thz time-max frq/time gap')
    plt.xlabel('Time (ps)')    
    plt.ylabel('Amplitude (arb. units)')
            
    plt.subplot(2,1,2)    
    plt.plot(x_mmgap.size, x_mmgap)
    plt.xlabel('time gap dt(ps)')    
    plt.show()
    

    
if __name__ == '__main__':
    main()
    
    